"""DOCUMENTATION UNFINISHED

GUI for comparing lift and drag frequency and amplitude for different 
datasets from unsteady CFD outputs. Currently, this script assumes 
that Cl is in the 7th column and Cd is in the 8th column of the data
(this is the output of plot_res.x from EDGE solver from FOI, Sweden).

Data should be provided to this script in the form of a "buttons.txt"
file, i.e.:
    # Selection name, /path/to/datafile, No. of grid cells, timestep)

This can be opened from the GUI or passed in the command line i.e.
    python plot_fft.py /path/to/buttons.txt

Previous data is saved in this files directory upon closing and will 
reopen on next opening of the program.

author:Paul Chambers
21/10/14
"""
import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import matplotlib.backends.backend_tkagg as tkagg
from matplotlib import rcParams
import sys
rcParams['font.family'] = 'serif'
rcParams['font.size'] = 12
rcParams['figure.figsize'] = (6,4)
import numpy as np
import os
try:
    #Python 2
    import Tkinter as tk
except ImportError:
    #Python 3 compatibility - package is named differently
    import tkinter as tk
import tkFileDialog as tk_fd

## DEFINE VARIABLES
# --------------------------------------------------------------------------
# Enter cylinder diameter (meters)
D = 0.01

# Enter freestream velocity U_inf (meters per second)
U = 34.03

# Enter Free stream density ('rho', kg/m^3) , viscosity ('mu', Ns/(m^-2))
# ... used to calculate Reynolds number
rho = 1.225
mu = 1.7894e-5
Re = rho * U * D / mu

# Reference Strouhal number (for table comparison)
S_ref = 0.228556502432   #Might add this as input later

# --------------------------------------------------------------------------
## DIRECTORY DEFS:
# --------------------------------------------------------------------------
basedir = os.path.dirname(os.path.abspath(sys.argv[0])) + '/'
# --------------------------------------------------------------------------

class button():
    """
    """
    def __init__(self, name, path, Num_cells, timestep):
        self.name = name
        self.path = path
        self.cells = Num_cells
        self.timestep = timestep

## GUI DEFINITIONS (for selecting residual files)
# --------------------------------------------------------------------------
class GUI(tk.Tk):
    """ GUI definitions:
    input variable btn_names is obtained before running GUI.mainloop
    """
    def __init__(self, button_data):
        tk.Tk.__init__(self)
        self.title('Plot FFT')
        # Create console Box
        self.console_text = tk.StringVar()
        self.console = tk.Label(self, textvariable=self.console_text).grid(row=5, column=0, columnspan=4)
        self.console_set(text="Loading GUI...")

        # Initialize figure area
        self.subfig, self.axes = plt.subplots(2,2, figsize=(14,6.5), facecolor='white')
        self.subfig.subplots_adjust(wspace=0.2, hspace=0.35)    #titles fit better 
        self.Cl_ax, self.Cl_fft_ax = self.axes[0, :]
        self.Cd_ax, self.Cd_fft_ax = self.axes[1, :]
        #formatting
        self.Cl_ax.set_label(r"$(t U)/ D$")
        self.Cl_ax.set_ylabel(r'$C_L$')
        self.Cl_ax.set_title('Lift Coefficient, Unsteady')
        self.Cl_fft_ax.set_xlabel('Frequency (Dimensionless) $f_s U / D$')
        self.Cl_fft_ax.set_ylabel(r'Amplitude')
        self.Cl_fft_ax.set_title(r'Lift Coefficient FFT')
        self.Cd_ax.set_xlabel(r"$(t U)/ D$")
        self.Cd_ax.set_ylabel(r'$C_D$')
        self.Cd_ax.set_title('Drag Coefficient, Unsteady')
        self.Cd_fft_ax.set_xlabel('Frequency (Dimensionless) $f U / D$')
        self.Cd_fft_ax.set_ylabel(r'Amplitude')
        self.Cd_fft_ax.set_title(r'Drag Coefficient FFT')
        self.plot_canvas = tkagg.FigureCanvasTkAgg(self.subfig, master=self)
        self.plot_canvas._tkcanvas.config(highlightthickness=0)
        self.plot_canvas.get_tk_widget().grid(column=0,row=0, sticky=('N','W','E','S'), columnspan=4)

        #Button for adding multiple files for selection
        tk.Label(text='Select data files for plotting:').grid(column=0, row=2, columnspan=2)
        loadButton = tk.Button(self, text="Load multiple", command=self.load_buttons)
        loadButton.grid(column=0, row=3)

        # Clear listbox button
        self.Clearbutton = tk.Button(text='Clear list', command=self.clear_buttons)
        self.Clearbutton.grid(column=1, row=3)

        # multiple selection listbox with scrollbar
        self.list = tk.Listbox(self, height=4, width=50, selectmode=tk.MULTIPLE)
        self.list.grid(column=0, row=4, sticky=('N', 'E', 'S'), columnspan=2)
        scroll = tk.Scrollbar(self, orient=tk.VERTICAL, command=self.list.yview)
        scroll.grid(column=1, row=4, sticky=('N','E', 'S'))
        self.list['yscrollcommand'] = scroll.set
        self.buttons = button_data  
        self.read_buttons()

        # Suffix Text Box
        suffix_lbl = tk.Label(text=' File name suffix:')
        suffix_lbl.grid(column=2, row=3, sticky='E')
        self.suffix = tk.Entry(self, width=25)
        self.suffix.grid(column=3, row=3, sticky='')

        #Button for saving plots
        self.save_int = tk.IntVar()
        saveButton = tk.Button(self, text="Save", command=self.save_selection)
        saveButton.grid(column=3, row=4, sticky='N')

        # Plot Button definition
        plotButton = tk.Button(self, text="Plot", command=self.plot_selection)
        plotButton.grid(column=3, row=2)

        # Redefine the function of the 'x' button for the window
        self.protocol("WM_DELETE_WINDOW", self.quit_command)
        self.console_text.set("Idle.")
    # --------------------------------------------------------------------------
    # GUI self.functions :
    def plot_selection(self):
        self.get_selections()
        self.save_int = 0
        figs = []
        if len(self.selection) == 0:
            self.console_set(text="Nothing to plot: select some data files")
        else:
            self.Cl_ax.clear(); self.Cd_ax.clear(), self.Cl_fft_ax.clear(), self.Cd_fft_ax.clear()
            self.console_text.set('Plotting...')
            self.update_idletasks()
            axes = plot_forces(get_residuals, self.selection, self.save_int, axes=(self.Cl_ax,self.Cl_fft_ax, self.Cd_ax,self.Cd_fft_ax))
            self.subfig.axes.append(axes)
            self.plot_canvas.draw()
            self.console_set()

    def load_buttons(self):
        """Load data from a plain text file in the appropriate format"""
        self.button_file = tk_fd.askopenfilename(filetypes = ( ("Plain text files", "*.txt"),("All files", "*.*"), ))
        self.buttons.extend(extract_buttondata(self.button_file))
        self.console_set(text='Loading file info...')
        self.read_buttons()
        self.console_set()

    def save_selection(self):
        """Plot and save currently selected figures as .eps"""
        self.savepath = tk_fd.askdirectory(title='Select an output directory') + '/'
        self.get_selections()
        self.save_int = 1
        suffix = self.suffix.get()
        if len(self.selection) == 0:
            print("Nothing to plot: select some data files")
        else:
            self.console_set(text='Saving high dpi figures (may take a while)...')
            figs = plot_forces(get_residuals, self.selection, self.save_int, suffix, self.savepath)
            self.console_set()

    def read_buttons(self):
        """read names of the loaded file buttons and append them to the listbox"""
        for i, button in enumerate(self.buttons):
            self.list.insert('end', button.name)

    def clear_buttons(self):     
        """Clear all buttons in the list box"""
        self.buttons = []
        self.list.delete(0, 'end')

    def get_selections(self):
        """Check which buttons are currently selected"""
        self.selection = []
        cur_sel = map(int,self.list.curselection())
        for i in cur_sel:
            self.selection.append(self.buttons[i])

    def console_set(self, text='Idle.'):
        """Deletes current entry in the console text box and prints console_idle"""
        self.console_text.set(text)
        self.update_idletasks()

    def quit_command(self):
        """ Quit command"""
        #save most recent buttons file:
        lines = ["{}, {}, {}, {}\n".format(btn.name, btn.path, btn.cells, btn.timestep) for btn in self.buttons]
        f = open(basedir+'buttons_autosave.txt', 'w')
        f.write("# Selection name, /path/to/datafile, No. of grid cells, timestep)\n")
        f.writelines(lines)
        f.close()
        os.rename(basedir+'buttons_autosave.txt', basedir+'buttons.txt')
        self.destroy()
        self.quit()
# --------------------------------------------------------------------------

# FUNCTION DEFINITIONS
# --------------------------------------------------------------------------
def get_residuals(filename, dt):
    """Extract and return data from the residual output
    filename and plot. Structure of 'filename' is:
    iteration, (some residual values), Cl in 7th col, Cd in 8th
    """
    data = np.loadtxt(filename)
    iteration = data[:,0]

    #Lift coefficient:
    Cl = data[:,6]    
    m = np.size(Cl)/2.
    #Take only the final half of values (ignore transient)
    Cl_mean = np.average(Cl[-m:]) 
    Cl_fft = np.fft.fft(Cl[-m:] - Cl_mean)    #remove mean from FFT
    Clfreqs = np.fft.fftfreq(Cl_fft.size, d=dt)
    #Find the index of the maximum bin in the Cl fft, then find the frequency.
    dominant_index = max(enumerate(np.abs(Cl_fft)),key=lambda x: x[1])[0] 
    dominant = abs(Clfreqs[dominant_index])

    #Drag coefficient:
    Cd = data[:,7]
    n = np.size(Cd)/2.
    #Take only the final half of values (ignore transient)
    Cd_mean = np.mean(Cd[-n:])
    Cd_fft = np.fft.fft(Cd[-n:] - Cd_mean)
    Cdfreqs = np.fft.fftfreq(Cd_fft.size, d=dt)
    #Find the index of the maximum bin in the Cl fft, then find the frequency.
    Cd_dominant_index = max(enumerate(np.abs(Cd_fft)),key=lambda x: x[1])[0] 
    Cd_dominant = abs(Cdfreqs[Cd_dominant_index])
    return iteration, Cl, Cl_fft, Clfreqs, dominant, Cd, Cd_fft, Cdfreqs, Cd_dominant


def plot_forces(get_residuals_func, selected_buttons, save_figs=0, suffix='', path=basedir, axes=()):
    """
    Calculates the FFT of the data in the files specified by selected_buttons
    (extracted by get_residuals_func) and plots the unsteady lift, drag and their fft

    Arguments
    ----------
    suffix - string
        suffix text for file names (defaults to empty)
    path - string
        path to save files (defaults to the install directory)
    axes - tuple
        Cl, Cl_fft, Cd and Cd_fft axes to plot to (gui)
    """
    N_cells = [int(button.cells) for button in selected_buttons]
    time_steps = [float(button.timestep) for button in selected_buttons]
    resid_files = [button.path for button in selected_buttons]

    freqs_nd = np.zeros(np.size(resid_files))
    Cdfreqs_nd = np.zeros_like(freqs_nd)
    Cl_max = np.zeros_like(freqs_nd)
    Cd_mean = np.zeros_like(freqs_nd)
    Cd_max = np.zeros_like(freqs_nd)

    #initialize figures
    plt.close("all")
    if save_figs==1:
        # Add \usepackage{lmodern} to latex preamble so that this allows nice figures
        rcParams['text.latex.preamble']=[r"\usepackage{lmodern}"]
        rcParams['font.family'] = 'lmodern'
        params = {'text.usetex' : True,
        'text.latex.unicode': True}
        rcParams.update(params)
        fig1 = plt.figure(1)
        fig2 = plt.figure(2)
        fig3 = plt.figure(3)
        fig4 = plt.figure(4)
        ax1 = fig1.add_subplot(111)
        ax2 = fig2.add_subplot(111)
        ax3 = fig3.add_subplot(111)
        ax4 = fig4.add_subplot(111)
        figs=[fig1, fig2, fig3, fig4]
    else:
        rcParams['font.family'] = 'serif'
        params = {'text.usetex' : False,
        'text.latex.unicode': False}
        rcParams.update(params)
        ax1, ax2, ax3, ax4 = axes

    if len(resid_files) != 0:
        for i, filename in enumerate(resid_files):
            tstep = time_steps[i]
            iteration, Cl, Cl_fft, Clfreqs, dominant, Cd, Cd_fft, Cdfreqs, Cd_dominant \
                = get_residuals_func(filename, tstep)

            m = np.size(Cl)/2.
            n = np.size(Cd)/2.
            Cl_max[i] = np.max(Cl[-m:])
            Cd_mean[i] = np.mean(Cd[-n:])
            Cd_max[i] = np.max(Cd[-n:])
            t = iteration*tstep * (U/D)
            Cdfreqs_nd[i] = Cd_dominant*D/float(U)
            freqs_nd[i] = dominant*D/float(U)
            nc = N_cells[i]
            lbl = r'{}k cells, dt {}$\mu$s'.format(nc/1000, tstep*10**6)
            # plot the signal and positive amplitude-frequency (first half) spectrum
            # only due to symmetry:
            # Cl
            ax1.plot(t-t[0], Cl, label=lbl)
            m = np.size(Clfreqs)/2
            ax2.semilogy(Clfreqs[1:m]*D/float(U), np.abs(Cl_fft)[1:m], label=lbl)
            # Cd
            ax3.plot(t-t[0],Cd, label=lbl)
            n = np.size(Cdfreqs)/2
            ax4.semilogy(Cdfreqs[1:n]*D/float(U), np.abs(Cd_fft[1:n]), label=lbl)

    #plot experimental upper and lower bounds on fft plot
    Cl_fft_low = 10**-1.5
    Cl_fft_high = 10**3
    ax2.semilogy([0.212]*2, [Cl_fft_low, Cl_fft_high], 'k-', label="Experiment region")
    ax2.semilogy([0.184]*2, [Cl_fft_low, Cl_fft_high], 'k-')
    
    #formatting
    ax1.set_xlabel(r"$(t U)/ D$")
    ax1.set_ylabel(r'$C_L$')
    ax1.set_title('Lift Coefficient, Unsteady')
    ax1.legend(loc='lower left', prop={'size':12})
    # ax1.set_xlim([0,50])
    ax1.set_ylim([-3, 2.0])
    ax2.set_xlim([0,max(Clfreqs)*D/float(U)])
    ax2.set_xlabel('Frequency (Dimensionless) $f_s U / D$')
    ax2.set_ylabel(r'Amplitude')
    ax2.set_title(r'Lift Coefficient FFT')
    ax2.legend(loc='upper right', prop={'size':12})
    ax2.set_xlim([0.0, 3.0])
    ax2.set_ylim([1, 1000])
    ax2.set_ylim([Cl_fft_low, Cl_fft_high])
    
    ax3.set_xlabel(r"$(t U)/ D$")
    ax3.set_ylabel(r'$C_D$')
    ax3.set_title('Drag Coefficient, Unsteady')
    ax3.legend(loc='lower right', prop={'size':12})
    ax3.set_xlim([0,50])
    # ax3.set_ylim([-0.2, 2.2])
    ax4.set_xlim([0,max(Clfreqs)*D/float(U)])
    ax4.set_xlabel('Frequency (Dimensionless) $f U / D$')
    ax4.set_ylabel(r'Amplitude')
    ax4.set_title(r'Drag Coefficient FFT')
    ax4.legend(loc='upper right', prop={'size':12})
    ax4.set_xlim([0,3])

    if save_figs == 1:
        Cl_file = path + "Cl" + suffix
        Cl_fft_file = path + "Cl_fft" + suffix
        Cd_file = path + "Cd" + suffix
        Cd_fft_file = path + "Cd_fft" + suffix
        print("Saving {}.eps".format(Cl_file))
        # fig1.savefig(Cl_file + '.png') #Saving pngs doesn't work with latex rcparams
        fig1.savefig(Cl_file + '.eps', dpi=1000, bbox_inches='tight')
        print("Saving {}.eps".format(Cl_fft_file))
        # fig2.savefig(Cl_fft_file + '.png')
        fig2.savefig(Cl_fft_file + '.eps', dpi=1000, bbox_inches='tight')
        print("Saving {}.eps".format(Cd_file))
        # fig3.savefig(Cd_file + '.png')
        fig3.savefig(Cd_file + '.eps', dpi=1000, bbox_inches='tight')
        print("Saving {}.eps".format(Cd_fft_file))
        # fig4.savefig(Cd_fft_file + '.png')
        fig4.savefig(Cd_fft_file + '.eps', dpi=1000, bbox_inches='tight')
    
    # Richardson extrapolation - to be implemented!
    # R = freqs_nd[0:8].reshape(2,4)
    # freqs_nd = freqs_nd.reshape(3,4)
    # #Richardson Extrapolate (fine and medium grids only):
    # extrap1 = ((4/3.)**-4*R[1,:] - R[0,:])/((4/3.)**-4-1)    #spacial extrapolation
    # I = (2**2*extrap1[0] - extrap1[1])/(2**2-1)    #temporal extrapolation
    # S_ref = I
    #Calculate errors
    eps = (freqs_nd - S_ref) / S_ref
    eps = eps.flatten()
    freqs_nd = freqs_nd.flatten()
    
    #Print the ASCII table comparing values
    print('Reference Strouhal number is {}:\n'.format(S_ref))
    print(' # Grid cells |time step (10^-6 s)| Cl_max  |  S_sim |(S_sim - S_ref / S_ref) (%)')
    print('--------------|-------------------|---------|--------|---------------------------')
    for i, freq in enumerate(freqs_nd):
        nc = N_cells[i]
        tstep = time_steps[i]
        print(' {:^12.0f} | {:^17.0f} | {:^7.4f} | {:^5.4f} | {:^20.3f} '.format(nc, tstep*10**6, Cl_max[i], freq, eps[i]*100))
     
    print("") 
    print("Drag Coefficient Frequencies (should be half Cl frequencies):")
    print(' # Grid cells |time step (10^-6 s)| Cd_mean | Cd_max | Frequency |')
    print('--------------|-------------------|---------|--------|-----------|')
    for i, Cd_freq in enumerate(Cdfreqs_nd):
        nc = N_cells[i]
        tstep = time_steps[i]
        print(' {:^12.0f} | {:^17.0f} | {:^7.4f} | {:^7.4f} | {:^9.4f} |'.format(nc, tstep*10**6, Cd_mean[i], Cd_max[i], Cd_freq))
    return ax1, ax2, ax3, ax4


def extract_buttondata(buttonfile):
    """
    Extracts file data in the 'buttons.txt' format to be used as selection buttons
    Arguments
    ---------
    buttonfile - string
         full path and name of file
    """
    try:
        fId = open(buttonfile, 'r')
    except IOError:
        "button file is not readable"
    btn_lines = fId.read().splitlines()[1:]
    fId.close()
    buttons = []

    if len(btn_lines) == 0:
        print("No data file info found in {}".format(buttonfile))
        print("Add some data files or open a saved file list")
    elif len(btn_lines[0].split(', ')) < 4:
        print("File list does not appear to be in the correct format")
        print("Add some data files or open a saved file list")
    else:
        for line in btn_lines:
            bits = line.split(', ')
            name = bits[0]
            path = bits[1]
            Num_cells = bits[2]
            timestep = bits[3]
            buttons.append(button(name, path, Num_cells, timestep))
    return buttons
# --------------------------------------------------------------------------


# MAIN PROGRAM STARTS HERE
# --------------------------------------------------------------------------
if __name__ == "__main__":
    ## SYSTEM COMMAND LINE arguments
    # --------------------------------------------------------------------------
    arguments = sys.argv
    if len(arguments) < 2:
        if 'buttons.txt' in os.listdir(basedir):
            print("Obtaining previous data file information...")
            buttonfile = basedir + 'buttons.txt'
        else:
            print("buttons.txt was not found in the base directory")
            print("creating empty file in {}".format(basedir))
            fId = open(basedir+'buttons.txt', 'w')
            fId.close()
    elif "--help" in arguments:
        print("Button file .txt should have the format:\n   \
    Button Name    path/to/forcedatafiles    no. cells    timestep(seconds)\n")
        quit()
    else:
        buttonfile = arguments[1]
    buttons = extract_buttondata(buttonfile)

    # --------------------------------------------------------------------------
    # run GUI:
    gui = GUI(button_data=buttons)
    gui.lift()
    gui.mainloop()
